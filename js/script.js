
/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth

(function ($, Drupal, window, document, undefined) {
    
    
	
    // Global var
    var GLOBAL = {};
    GLOBAL.resizeTimer;
    GLOBAL.isMobile = false;
	GLOBAL.isSmallTabletPortrait = false;
	GLOBAL.deviceHasChanged = false;
	GLOBAL.tabletHasChanged = false;
	GLOBAL.showNav = function (){
		$('#nav').show();
        $('#menu-toggle').attr('class','close');
		$('.dropdown').removeClass('open');
		$('.dropdown ul').removeAttr('style');
	};
	GLOBAL.hideNav = function (){
		$('#nav').hide();
		$('#menu-toggle').attr('class','open');
		$('.dropdown').removeClass('open');
	};
	

	
    // Windows Resize Behavior
    Drupal.behaviors.windowResizingBehavior = {
        attach: function(context, settings) {
            $(window).resize(function() {
                clearTimeout(GLOBAL.resizeTimer);
                GLOBAL.resizeTimer = setTimeout(function() { Drupal.attachBehaviors(); }, 100);
            });
        }
    };
	
	
	
	// Check if mobile device based on #menu-toggle display on media queries
    Drupal.behaviors.loadNav = {
        attach: function(context, settings) {
            
			$('body').once('loadNav', function() {
			
				xhr = $.ajax({
					type: 'GET',
					url: 'http://collagensolutions.com',
					data: { response_type: 'ajax' },
					success: function(result) {
						
						var mainNav = $(result).find('#block-menu-block-1');
						var footerNav = $(result).find('.footer-menu').html();
						
						$('#nav').html(mainNav);
						$('.footer-menu').html(footerNav);
						
						$('#nav li').has('ul').addClass('dropdown');
						$('.dropdown > a').removeAttr('href').click(function(){
							ul = $(this).next();
							li = $(this).parent();
							if(GLOBAL.isMobile){
								if(ul.is(':visible')){
									ul.slideUp(600, "easeOutQuart");
									li.removeClass('open');
								}else{
									ul.slideDown(600, "easeOutQuart");
									li.addClass('open');
								}
							}
						});
						
						$('.menu-mlid-765').addClass('is-active-trail');
						$('.menu-mlid-765 > a').addClass('active');
						
						$('#nav a[href^="/"], .utility a[href^="/"], .footer-menu a[href^="/"]').each( function(){
							var href = $(this).attr('href');
							$(this).attr('href','http://www.collagensolutions.com'+href);
						});
						
					}
				});
			
			});

        }
    };
	
	


    
    // Check if mobile device based on #menu-toggle display on media queries
    Drupal.behaviors.checkIfMobile = {
        attach: function(context, settings) {
            
			if ( $("#menu-toggle").css("display") === 'block') {
                if( GLOBAL.isMobile ){
					GLOBAL.deviceHasChanged = false;
				}else{
				   GLOBAL.deviceHasChanged = true; 
				   GLOBAL.isMobile = true;
				   GLOBAL.hideNav();
				}			
            }else{
                if( !GLOBAL.isMobile ){
					GLOBAL.deviceHasChanged = false;
				}else{
					GLOBAL.deviceHasChanged = true;
					GLOBAL.isMobile = false;
					GLOBAL.showNav();
				}
				
            }

        }
    };
	
	
	
	// Global interface bits
    Drupal.behaviors.interface = {
        attach: function(context, settings) {
            
            $('#page').once('interface', function() {
				
				// Parallax
				$(window).scroll(function(){
				  var position = $(window).scrollTop();
				  $('.parallax').css('background-position','center '+position/2+'px');
				});
				
				// Match Heights
				$('#feeds .grid').matchHeight();
				
				// Footer Menu
				$('#block-menu-block-1--2 > div > ul > li > a').removeAttr('href');
				
			});
			
		}
	}

	
	
	// Navigation Behavior
    Drupal.behaviors.navigation = {
        attach: function(context, settings) {
            
            $('#page').once('navigation', function() {

				$("#menu-toggle").on( 'click', function(event){
					event.preventDefault();
					if( $(this).hasClass("open") ){
						$("#nav").slideDown(600, "easeOutQuart");
						$(this).attr('class','close');
					}else{
						$("#nav").slideUp(600, "easeOutQuart");
						$(this).attr('class','open');
					}
				});
		
				$('#nav li').has('ul').addClass('dropdown');

				$('.dropdown > a').removeAttr('href').click(function(){
					ul = $(this).next();
					li = $(this).parent();
					if(GLOBAL.isMobile){
						if(ul.is(':visible')){
							ul.slideUp(600, "easeOutQuart");
							li.removeClass('open');
						}else{
							ul.slideDown(600, "easeOutQuart");
							li.addClass('open');
						}
					}
				});
								
 
            });
   
        }
    };
	
	
	
	// Table for small screens
    Drupal.behaviors.responsiveTable = {
        attach: function(context, settings) {
            
            $('body:not(.ir_news) #page table').once('responsiveTable', function() {
 
                $("#page table:not(.ignore) td").each(function(index) {
                    
                    // If 2 column table, do not add responsive table class
                    var nbTd = $(this).siblings().andSelf().length;
                    if(nbTd !== 2 && nbTd !== 1){
                        $(this).parents('table').addClass("responTable");
                    }
  
                    // Get label of td and add as html5 data attr
                    var indexTd = $(this).index();
                    var textTh = $.trim($(this).parent().parent().parent().find("thead tr th").eq(indexTd).text());
                    if (textTh === "")$(this).addClass("empty-row");
                    $(this).attr("data-label",textTh);
					
					// Hide empty cells
					if($(this).html() == '-'){
						$(this).addClass('empty-cell');
					}
                
                });
            
            });

        }
    };




})(jQuery, Drupal, this, this.document);

